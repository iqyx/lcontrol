const config = {
	ui: {
		reset_zone_config_on_click: true,
		change_zone_config_on_click: true,
		open_zone_config_on_click: false,
		privileged_pin: "0000"
	},
	svg: {
		width: 1080,
		height: 2000,
		touch_response: false,
		inactive: {
			stroke: "#888",
			stroke_width: 0.3,
			fill: "#fff"
		},
		active: {
			stroke: "#ff9a06",
			stroke_width: 2
		},
		highlight: {
			fill: "#ff0"
		}
	}
};

