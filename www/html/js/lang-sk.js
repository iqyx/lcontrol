const lang = {
	Menu: "Menu",
	Osvetlenie: "Ovládací panel osvetlenia",
	Effects: "Voľba efektu zóny",
	Scenes: "Uložené scény",
	LOCK_SCREEN_LEFT: "zostáva",
	LOCK_SCREEN_MINUTES: "minút",
	lock_screen_left: "zostáva",
	lock_screen_minutes: "minút",
	modal_screen_locked: "Ovládanie uzamknuté iným zariadením"
};
