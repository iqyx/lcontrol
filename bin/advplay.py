#!/usr/bin/python3

# run vlc as
# vlc -I rc --rc-host localhost:8080 --no-mouse-events --loop --no-osd --vout gles2 --fullscreen

import argparse
import json
import paho.mqtt.client
import time
import queue
import socket
import os

zones = {}

def zonePlay(zone):
	try:
		sendCommand(f'stop')
		sendCommand(f'clear')

		for (root, dirs, files) in os.walk(f'{args.dir}/{zone}/'):
			for fname in files:
				sendCommand(f'add {root}/{fname}')

		sendCommand(f'play')
	except Exception as e:
		print(str(e))

def sendCommand(cmd):
	print(f'command {cmd}')
	s = socket.create_connection((args.vlc_host, args.vlc_port), timeout=0.1)
	s.sendall(cmd.encode('utf-8'))
	s.sendall(b'\r\n')
	s.close()

def zoneUpdate(zone, val):
	print(f'zone update {zone}={val} zones={list(zones.values())}')
	if val:
		zonePlay(zone)
	if not any(zones.values()):
		zonePlay('default')


def mqttMessage(client, userdata, msg):
	# Message topic starts with args.topic, because these topics are the only ones we are subscribed to
	# Strip the topic prefix first.
	t = msg.topic.removeprefix(args.topic)

	(zone, suffix) = t.split('/', 2)
	current_val = zones.get(zone)
	new_val = args.state_default
	if args.state_toggle and (args.state_toggle in msg.payload.decode('utf-8')):
		new_val = not new_val

	if current_val != new_val:
		zones[zone] = new_val
		zoneUpdate(zone, new_val)


parser = argparse.ArgumentParser(description='VLC controller/advertisement player')
parser.add_argument('--host', type=str, default='localhost', help='hostname of the MQTT message broker')
parser.add_argument('-p', '--port', type=int, default=1883, help='port of to MQTT message broker')
parser.add_argument('-c', '--config', type=str, default='advplay.json', help='channel configuration file')
parser.add_argument('-t', '--topic', type=str, default='zone/', help='topic prefix for input zones')

parser.add_argument('--vlc-host', type=str, default='localhost', help='hostname of VLC RC interface')
parser.add_argument('--vlc-port', type=int, default=8080, help='port of VLC RC interface')
parser.add_argument('--dir', type=str, default='media/', help='playlist directory prefix')
parser.add_argument('--state-default', type=bool, default=False, help='default zone state')
parser.add_argument('--state-toggle', type=str, default='', help='switch zone state if a substring is found')

args = parser.parse_args()


# Connect to the MQTT broker and start listening for zone feedback (update) messages
mqttc = paho.mqtt.client.Client(client_id='advplay')
mqttc.on_message = mqttMessage
mqttc.connect(args.host, args.port, 60)
mqttc.subscribe(args.topic + '+/mode', 1)
mqttc.loop_start()

zonePlay('default')

while True:
	try:
		time.sleep(1)
	except KeyboardInterrupt:
		break

mqttc.loop_stop()
print()
exit(0)
