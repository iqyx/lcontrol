#!/usr/bin/python3

import argparse
import json
import paho.mqtt.client
import time
import rpi_ws281x
import colour

def rgb_ch_value(s):
	try:
		c = colour.Color(s.decode('utf-8'))
		return c.rgb + (c.luminance,)
	except Exception as e:
		print(str(e))
		return (0.0, 0.0, 0.0, 0.0)


# Prisla message na MQTT broker, ktora nas zaujima (predtym sme sa na nu subscribli).
# Zistime, ci vieme naparsovat nazov kanalu a najdime ho v konfiguracii. Ak taky
# kanal mame v konfiguracii, zistime jeho novu hodnotu a priradime ju do pola kanalov.
def mqtt_on_message(client, userdata, msg):
	channel = int(msg.topic.split('/')[-1])
	(r, g, b, w) = rgb_ch_value(msg.payload)
	if args.strip == 'rgb':
		strip.setPixelColor(channel, rpi_ws281x.Color(int(r * 255.0), int(g * 255.0), int(b * 255.0)))
	if args.strip == 'rgbw':
		if r == g == b:
			r = g = b = 0.0
		else:
			w = 0.0
		strip.setPixelColor(channel, rpi_ws281x.Color(int(r * 255.0), int(g * 255.0), int(b * 255.0), int(w * 255.0)))

# Parsovanie parametrov z prikazoveho riadku. Sluzia na konfiguraciu vsetkeho okrem
# samotnych kanalov.
parser = argparse.ArgumentParser(description='WS281x output driver')
parser.add_argument('--host', type=str, default='localhost', help='hostname of the MQTT message broker')
parser.add_argument('-p', '--port', type=int, default=1883, help='port of to MQTT message broker')
parser.add_argument('-t', '--topic', type=str, default='channel/#', help='subscribe MQTT topic filter for the channel output, last component of the topic is the channel name')
parser.add_argument('-l', '--length', type=int, default=16, help='length of the WS281x strip (number of LEDs)')
parser.add_argument('-s', '--strip', type=str, default="rgb", help='rgb (24 bit) or rgbw (32 bit) strip')
args = parser.parse_args()

# Skusime sa pripojit na MQTT broker a prihlasime sa na odber sprav podla --topic
mqttc = paho.mqtt.client.Client()
mqttc.connect(args.host, args.port, 60)
mqttc.on_message = mqtt_on_message
mqttc.subscribe(args.topic, 0)
mqttc.loop_start()

strip_type = rpi_ws281x.ws.SK6812W_STRIP if args.strip == "rgbw" else rpi_ws281x.ws.WS2812_STRIP
strip = rpi_ws281x.PixelStrip(args.length, 21, 880000, 10, False, 255, 0, strip_type)
strip.begin()

while True:
	try:
		strip.show()
		time.sleep(0.05)
	except:
		break

# Upraceme po sebe a koncime
mqttc.disconnect()
print()
exit(0)







	
