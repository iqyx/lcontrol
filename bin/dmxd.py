#!/usr/bin/python3

import argparse
import json
import paho.mqtt.client
import time
import serial
import colour


# Prisla message na MQTT broker, ktora nas zaujima (predtym sme sa na nu subscribli).
# Zistime, ci vieme naparsovat nazov kanalu a najdime ho v konfiguracii. Ak taky
# kanal mame v konfiguracii, zistime jeho novu hodnotu a priradime ju do pola kanalov.
def mqtt_on_message(client, userdata, msg):
	channel = msg.topic.split('/')[-1]
	chc = config.get(channel)
	if chc:
		if chc['address'] > args.maxch:
			return

		c = colour.Color(msg.payload.decode('utf-8'))
		if chc['type'] == 'single':
			channels[chc['address']] = c.luminance
		if chc['type'] == 'rgb':
			channels[chc['address']] = c.red
			channels[chc['address'] + 1] = c.green
			channels[chc['address'] + 2] = c.blue


# Parsovanie parametrov z prikazoveho riadku. Sluzia na konfiguraciu vsetkeho okrem
# samotnych kanalov.
parser = argparse.ArgumentParser(description='DMX512 output driver')
parser.add_argument('--host', type=str, default='localhost', help='hostname of the MQTT message broker')
parser.add_argument('-p', '--port', type=int, default=1883, help='port of to MQTT message broker')
parser.add_argument('-s', '--serial', type=str, default='/dev/ttyUSB0', help='serial port for DMX output')
parser.add_argument('-t', '--topic', type=str, default='channel/#', help='subscribe MQTT topic filter for the channel output, last component of the topic is the channel name')
parser.add_argument('-c', '--config', type=str, default='dmxd.json', help='channel configuration file')
parser.add_argument('-m', '--maxch', type=int, default=64, help='maximum channel number')
args = parser.parse_args()

# By default su vsetky kanaly tmave. Prvy kanal sa nepouziva, musi byt 0
channels = [0] * args.maxch

# Nacitame konfiguraciu kanalov z konfiguraku v JSON formate
config = {}
with open(args.config, 'r') as f:
	config = json.load(f)

# Skusime sa pripojit na MQTT broker a prihlasime sa na odber sprav podla --topic
mqttc = paho.mqtt.client.Client()
mqttc.connect(args.host, args.port, 60)
mqttc.on_message = mqtt_on_message
mqttc.subscribe(args.topic, 0)
mqttc.loop_start()

# Otvorime a nakonfigurujeme seriovy port pre DMX512 vystup
ser = serial.Serial(args.serial, 250000, bytesize=8, parity="N", stopbits=2)

# A donekonecna posielame hodnoty vsetkych kanalov cez DMX, az kym nenastane
# nejaky problem (napr. KeyboardInterrupt)
while True:
	try:
		# ser.send_break(duration=0.001)
		ser.break_condition = True
		time.sleep(0.001)
		ser.break_condition = False
		
		ser.write(bytes([int(ch * 255) for ch in channels]))
		time.sleep(0.001)
	except KeyboardInterrupt:
		break

# Upraceme po sebe a koncime
ser.close()
mqttc.disconnect()
print()
exit(0)

