#!/usr/bin/python3

import argparse
import json
import paho.mqtt.client
import time
import colour
import queue
import cProfile


# range_to() metoda objektu Color v colour libke robi prechody zle,
# vid PR na https://github.com/vaab/colour/pull/50. Pouzili sme teda
# vlastnu poor man metodu, ktora prechod interpoluje v RGB priestore
def color_range(c_from, c_to, n):
	(r1, g1, b1) = colour.Color(c_from).rgb
	(r2, g2, b2) = colour.Color(c_to).rgb

	rd = (r2 - r1) / (n - 1)
	rg = (g2 - g1) / (n - 1)
	rb = (b2 - b1) / (n - 1)

	cs = []
	for i in range(n):
		c = colour.Color(red=(r1 + rd * i), green=(g1 + rg * i), blue=(b1 + rb * i))
		cs.append(c)

	return cs


class LController(object):
	pass


class LCSwitch(LController):

	MODE_OFF = 1
	MODE_ON = 2
	MODE_FADE = 3
	MODE_JUMP = 4

	def __init__(self, name, config):
		self._name = name
		self._config = config
		self._queue = queue.Queue()
		self._last_c = colour.Color('black')
		self._mode = LCSwitch.MODE_OFF
		if self._config.get('max-brightness') != None:
			self._max_brightness = float(self._config['max-brightness'])
		else:
			self._max_brightness = 1
		self._ov_color = self._config.get('override-color')

		self._colors = [colour.Color('white')]
		self._brightness = 1.0
		self._speed = 0.5
		self._zone = self._config['zone']

		self._last_output = None
		self._last_feedback = None
		self._steps_since_last_feedback = 0


	def message(self, msg):
		if msg.topic == "%s%s/mode" % (args.topic, self._zone):
			if msg.payload.decode('utf-8') == 'off':
				self._transitionTo(colour.Color('black'), 2)
				self._mode = LCSwitch.MODE_OFF

			if msg.payload.decode('utf-8') == 'on':
				self._transitionTo(self._colors[0], 2)
				self._mode = LCSwitch.MODE_ON

			if msg.payload.decode('utf-8') == 'fade':
				self._mode = LCSwitch.MODE_FADE

			if msg.payload.decode('utf-8') == 'jump':
				self._mode = LCSwitch.MODE_JUMP

		if msg.topic == "%s%s/color" % (args.topic, self._zone):
			cstr = msg.payload.decode('utf-8')
			if self._ov_color:
				cstr = self._ov_color
			self._colors = [colour.Color(c) for c in cstr.split(';')]
			if self._mode == LCSwitch.MODE_ON:
				self._transitionTo(self._colors[0], 10)

		if msg.topic == "%s%s/speed" % (args.topic, self._zone):
			self._speed = float(msg.payload.decode('utf-8'))

		if msg.topic == "%s%s/brightness" % (args.topic, self._zone):
			self._brightness = float(msg.payload.decode('utf-8'))


	def _transitionTo(self, new_color, l):
		for c in color_range(self._last_c, new_color, l):
			self._queue.put(c)
			self._last_c = c


	def _setOutputs(self, c):
		if c == self._last_output:
			return

		self._last_output = c
		for o in self._config['outputs']:
			mqttc.publish(o, c, qos=0)


	def _sendFeedback(self, c):
		self._steps_since_last_feedback += 1

		if (c != self._last_feedback) or self._steps_since_last_feedback > 200:
			# but not too soon!
			if self._steps_since_last_feedback < 20:
				return
			mqttc.publish(args.topic + self._zone + '/feedback', c, qos=1)
			self._steps_since_last_feedback = 0
			self._last_feedback = c


	def step(self):
		# Najjednoduchsi sposob, ako casovat naplnenie queue s farbami pre FADE a JUMP
		# mody je pri vyprazdneni queue. Musime to urobit ale skor, ako zistime, ze je prazdna.
		if self._queue.empty():
			if self._mode == LCSwitch.MODE_FADE:
				self._transitionTo(self._colors[0], int(200 * (1 - self._speed) + 2))
				self._colors = self._colors[1:] + self._colors[:1]

			if self._mode == LCSwitch.MODE_JUMP:
				self._transitionTo(self._colors[0], 2)
				self._transitionTo(self._colors[0], int(200 * (1 - self._speed) + 2))
				self._colors = self._colors[1:] + self._colors[:1]

		if self._queue.empty():
			c = self._last_c
		else:
			c = self._queue.get()

		# Adjust the maximum brightness before setting the colour
		c_br = colour.Color(c)
		c_br.luminance = c_br.luminance * self._brightness * self._max_brightness

		self._setOutputs(str(c_br))
		self._sendFeedback(str(c_br))


def mqttMessage(client, userdata, msg):
	# Message topic starts with args.topic, because these topics are the only ones we are subscribed to
	# Strip the topic prefix first.
	t = msg.topic.removeprefix(args.topic)

	(zone, suffix) = t.split('/', 2)

	# Controllers are organized by zone names. Get the name and pass the message
	# to the right controller.
	for controller in controllers.values():
		if controller._zone == zone:
			controller.message(msg)


parser = argparse.ArgumentParser(description='WS281x output driver')
parser.add_argument('--host', type=str, default='localhost', help='hostname of the MQTT message broker')
parser.add_argument('-p', '--port', type=int, default=1883, help='port of to MQTT message broker')
parser.add_argument('-c', '--config', type=str, default='lcontrold.json', help='channel configuration file')
parser.add_argument('-t', '--topic', type=str, default='zone/', help='topic prefix for input zones')
args = parser.parse_args()

config = {}
with open(args.config, 'r') as f:
	config = json.load(f)

mqttc = paho.mqtt.client.Client(client_id='lcontrold')
mqttc.on_message = mqttMessage
mqttc.connect(args.host, args.port, 60)
mqttc.subscribe(args.topic + '+/mode', 1)
mqttc.subscribe(args.topic + '+/color', 1)
mqttc.subscribe(args.topic + '+/speed', 1)
mqttc.subscribe(args.topic + '+/brightness', 1)

controllers = {}
for cname, cconfig in config.get('controllers').items():
	if cconfig['type'] == "switch":
		sw = LCSwitch(cname, cconfig)
		# Organize by zone to speed up lookup
		controllers[cname] = sw

mqttc.loop_start()

while True:
	try:
		for c in controllers.values():
			c.step()
		time.sleep(0.02)
	except KeyboardInterrupt:
		break

mqttc.loop_stop()
print()
exit(0)








