#!/usr/bin/python3

import argparse
import json
import paho.mqtt.client
import time
import serial
import colour
import gpiozero


def mqtt_on_message(client, userdata, msg):
	tt = msg.topic.split('/')
	if tt[-1] == 'mode':
		zona = '/'.join(tt[:-1])
		zone_state[zona] = msg.payload.decode('utf-8')



parser = argparse.ArgumentParser(description='DMX512 output driver')
parser.add_argument('--host', type=str, default='localhost', help='hostname of the MQTT message broker')
parser.add_argument('-p', '--port', type=int, default=1883, help='port of to MQTT message broker')
parser.add_argument('-c', '--config', type=str, default='dmxd.json', help='channel configuration file')
args = parser.parse_args()

zone_state = {}
button_state = {}
config = {}
with open(args.config, 'r') as f:
	config = json.load(f)

mqttc = paho.mqtt.client.Client()
mqttc.connect(args.host, args.port, 60)
mqttc.on_message = mqtt_on_message
mqttc.subscribe('zone/#', 1)
mqttc.loop_start()

# Nakonfiguruj vsetky vstupy z konfiguraku na input, pull-up
for zona in config:
	config[zona]['button'] = gpiozero.Button(config[zona]['gpio'])
	zone_state[zona] = 'off'
	button_state[zona] = False

while True:

	try:
		for zona in config:
			p = config[zona]['button'].is_pressed

			# on release
			if p != button_state[zona] and p == False:

				if zona == "*":
					for z in config:
						if z != "*":
							zone_state[z] = config[zona]['mode']
							mqttc.publish(z + '/color', config[z]['color'], qos=2)
							mqttc.publish(z + '/mode', zone_state[z], qos=2)
				else:
					if zone_state[zona] == 'off':
						zone_state[zona] = config[zona]['mode']
					else:
						zone_state[zona] = 'off'

					mqttc.publish(zona + '/color', config[zona]['color'], qos=2)
					mqttc.publish(zona + '/mode', zone_state[zona], qos=2)
				
			button_state[zona] = p

		time.sleep(0.1)
	except KeyboardInterrupt:
		break

mqttc.disconnect()
print()
exit(0)

