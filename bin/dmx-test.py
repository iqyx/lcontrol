#!/usr/bin/python3

import serial


with serial.Serial("/dev/ttyAMA0", 250000, bytesize=8, parity="N", stopbits=2) as s:
	s.send_break()
	s.write(b"\x00")
	s.write(b"\xff")
	for i in range(511):
		s.write(b"\x00")

